/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.werapan.databaseproject.dao.OrdersDao;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Orders;
import com.werapan.databaseproject.model.Product;

/**
 *
 * @author acer
 */
public class TestOrder {

    public static void main(String[] args) {
        Product prodoct1 = new Product(1, "A", 75);
        Product prodoct2 = new Product(2, "B", 180);
        Product prodoct3 = new Product(3, "C", 70);
        Orders order = new Orders();
//        OrderDetail orderDetail1 = new OrderDetail(prodoct1, prodoct1.getName(), prodoct1.getPrice(), 1, order);
        //Add Product
        order.addOrderDetail(prodoct1, 1); // product ,qty
        order.addOrderDetail(prodoct2, 1); // product ,qty
        order.addOrderDetail(prodoct3, 1);// product ,qty
        System.out.println(order);
        System.out.println(order.getOrderDetails());
        printReciept(order);
        
        OrdersDao orderDao = new OrdersDao();
//        orderDao.save(order);
        Orders neworder = orderDao.save(order);
       System.out.println(neworder);

//        System.out.println(orderDao.get(3));

    }

    static void printReciept(Orders order) {
        System.out.println("Order " + order.getId());
        for (OrderDetail orderDetail : order.getOrderDetails()) {
            System.out.println(" " + orderDetail.getProductname() + " " + orderDetail.getQty() + " " + orderDetail.getProduPrice() + orderDetail.getTotal());
        }
        System.out.println("Total: "+order.getTotal()+ "  Qty: "+order.getQty());
    }
}
